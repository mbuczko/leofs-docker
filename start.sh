#!/bin/bash
leopath=/opt/leofs

manager_0=$leopath/leo_manager_0/bin/leo_manager
manager_1=$leopath/leo_manager_1/bin/leo_manager
storage=$leopath/leo_storage/bin/leo_storage
gateway=$leopath/leo_gateway/bin/leo_gateway

ip=`ifconfig eth0 2>/dev/null|awk '/inet addr:/ {print $2}'|sed 's/addr://'`

proto=$PROTO
manager=$MANAGER

if [[ ! -z $manager ]]; then
  sed -i "s/@127.0.0.1/@$manager/g" $leopath/leo_manager_0/etc/leo_manager.conf
  sed -i "s/@127.0.0.1/@$manager/g" $leopath/leo_manager_1/etc/leo_manager.conf
  sed -i "s/managers = .*$/managers = [manager_0@$manager, manager_1@$manager]/g" $leopath/leo_storage/etc/leo_storage.conf
  sed -i "s/managers = .*$/managers = [manager_0@$manager, manager_1@$manager]/g" $leopath/leo_gateway/etc/leo_gateway.conf
fi
if [[ -z $proto ]]; then
   proto="s3"
fi

sed -i "s/nodename = .*$/nodename = gateway_0@$ip/g" $leopath/leo_gateway/etc/leo_gateway.conf
sed -i "s/nodename = .*$/nodename = storage_0@$ip/g" $leopath/leo_storage/etc/leo_storage.conf

sed -i "s/protocol = .*$/protocol = $proto/g" $leopath/leo_gateway/etc/leo_gateway.conf

if [[ ! -z $CONSISTENCY_REPLICAS ]]; then
    echo "setting number of consistency replicas to $CONSISTENCY_REPLICAS"
    sed -i "s/consistency.num_of_replicas = .*$/consistency.num_of_replicas = $CONSISTENCY_REPLICAS/g" $leopath/leo_manager_0/etc/leo_manager.conf
fi
for c in $RUN
do
    echo "running ${!c}"
    ${!c} start
done
tail -f $leopath/start.sh
