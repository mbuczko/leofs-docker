recognized environment variables:

 - RUN=<services> services to run within a node. possible ones are: manager_0 manager_1 storage gateway
 - CONSISTENCY_REPLICAS=<no_of_replicas> number of replicas for consistent write
 - MANAGER=<manager_node> manager node reachable by all other nodes within overlay network
 - PROTO=<protocol> protocol handled by gateways: rest or s3.

to have LeoFS clustered its nodes need to run within an overlay network:

    docker network create --driver overlay leonet

to create manager nodes:

    docker service create --network leonet --name leofs-manager -e "CONSISTENCY_REPLICAS=2" -e "RUN=manager_0 manager_1" -e "MANAGER=leofs-manager.leonet" quay.io/defunkt/leofs

to create storage node:

    docker service create --network leonet --name leofs-storage --replicas 2 -e "RUN=storage" -e "MANAGER=leofs-manager.leonet" quay.io/defunkt/leofs

to create gatewat node with REST or S3 protocol:

    docker service create --network leonet -p 8080:8080 -p 8443:8443 --name leofs-gateway --replicas 2 -e "RUN=gateway" -e "MANAGER=leofs-manager.leonet" -e "PROTO=rest" quay.io/defunkt/leofs

for development only:

    docker run -d -p 8080:8080 -p 8443:8443 --name leofs -e "CONSISTENCY_REPLICAS=1" -e "RUN=manager_0 manager_1 storage gateway" quay.io/defunkt/leofs
