FROM alpine:3.5
MAINTAINER Michal Buczko "michal.buczko@gmail.com"

ENV LEOFS=/opt/leofs

RUN adduser -S leofs
RUN apk --no-cache --update upgrade && \
    apk add --no-cache tini openssl netcat-openbsd ca-certificates ncurses snappy net-snmp-libs unixodbc check less bash sudo
RUN apk add --no-cache --virtual .build-deps gcc g++ make cmake openssl-dev ncurses bsd-compat-headers autoconf \
        ncurses-dev unixodbc-dev check-dev snappy-dev libc-dev zlib-dev git

RUN git clone -b OTP-18.3.4.4 --single-branch --depth 1 https://github.com/erlang/otp.git /tmp/erlang-build && cd /tmp/erlang-build && \
    wget http://www.ivmaisoft.com/_bin/atomic_ops/libatomic_ops-7.4.4.tar.gz && \
    tar xzvf libatomic_ops-7.4.4.tar.gz && cd libatomic_ops-7.4.4 && ./configure --prefix=/usr/local && make && make install && cd .. && \
    export ERL_TOP=/tmp/erlang-build && \
    export PATH=$ERL_TOP/bin:$PATH && \
    export CPPFlAGS="-D_BSD_SOURCE $CPPFLAGS" && \
    ./otp_build autoconf && \
    ./configure --prefix=/usr \
      --sysconfdir=/etc \
      --mandir=/usr/share/man \
      --infodir=/usr/share/info \
      --without-javac \
      --without-debugger \
      --without-jinterface \
      --without-common_test \
      --without-cosEvent\
      --without-cosEventDomain \
      --without-cosFileTransfer \
      --without-cosNotification \
      --without-cosProperty \
      --without-cosTime \
      --without-cosTransactions \
      --without-dialyzer \
      --without-et \
      --without-ic \
      --without-megaco \
      --without-orber \
      --without-percept \
      --without-typer \
      --enable-threads \
      --enable-shared-zlib \
      --disable-hipe \
      --with-libatomic_ops=/usr/local && \
    make -j4 && make install

RUN mkdir /opt && \
    cd /tmp && git clone https://github.com/leo-project/leofs.git && \
    cd leofs && make && make release && mv package ${LEOFS} && \
    cd .. && rm -rf leofs erlang-build

RUN apk del .build-deps && update-ca-certificates --fresh

ADD start.sh ${LEOFS}/start.sh
RUN chmod ug+x ${LEOFS}/start.sh
RUN chown -R leofs ${LEOFS}

ENTRYPOINT ["/sbin/tini", "--"]
WORKDIR ${LEOFS}

EXPOSE 8080
EXPOSE 8443

CMD ["/opt/leofs/start.sh"]
